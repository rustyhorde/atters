// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! proxy-api query definitions.
mod corrid;
mod path;

pub use self::corrid::ProxyApiCorrId;
pub use self::path::ProxyApiDest;

/// `echo_id`, `correlation-id`, `timestamp`, `destination_path`, `response_code`, `ip`, `payload_size`, `bytes_sent`, `duration`.
pub type InsertTuple = (String, String, i64, String, u16, String, i64, i64, i64);

/// The hit source data.
#[derive(Debug, Deserialize, Getters)]
#[serde(rename_all = "camelCase")]
pub struct Source {
    /// Correlation ID
    #[get = "pub"]
    correlation_id: String,
    /// Timestamp
    #[get = "pub"]
    timestamp: i64,
    /// Destination Path
    #[get = "pub"]
    destination_path: String,
    /// Response Code
    #[get = "pub"]
    response_code: u16,
    /// IP address
    #[get = "pub"]
    #[serde(rename = "destinationHostName")]
    ip: String,
    /// Payload size
    #[get = "pub"]
    payload_size: i64,
    /// Message detail block.
    #[get = "pub"]
    message_detail: MessageDetail,
    /// Duration
    #[get = "pub"]
    duration: i64,
}

/// Message detail block
#[derive(Debug, Deserialize, Getters)]
#[serde(rename_all = "camelCase")]
pub struct MessageDetail {
    /// Bytes sent.
    #[get = "pub"]
    bytes_sent: i64,
}
