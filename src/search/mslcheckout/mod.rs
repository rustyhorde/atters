// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! mobile-service-layer-checkout searcher
use atter::{self, Config, EchoIndex};
use error::Result;
use libeko::{self, Client, EchoSearchClient};
use regex::Regex;
use rusqlite::Connection;
use search::{Bool, ConstantScore, Filter, Matches, Param, Query, Range, RequestBody, Scroll,
             Searcher, Timestamp};
use sender::SearchConfig;
use serde_json;
use std::collections::HashMap;
use utils;

lazy_static! {
    static ref CREATE: Regex = Regex::new(r"^POST /mobilecheckout/api/v(\d{1})/order/create$")
        .expect("unable to create CREATE regex");
    static ref RELEASE: Regex = Regex::new(r"^POST /mobilecheckout/api/v(\d{1})/order/release$")
        .expect("unable to create RELEASE regex");
}

/// `echo_id`,         `correlation-id`,  `profile_id`,     `timestamp`,
/// `version`,         `endpoint`,        `response_code`,  `fulfillment`,
/// `master_order_id`, `suborder`,        `user_agent`,     `problem_area`,
/// `validation_code`, `internal_message`.
pub type InsertTuple = (
    String,
    String,
    String,
    i64,
    u8,
    String,
    u16,
    String,
    String,
    String,
    String,
    String,
    String,
    String,
);

/// `count`, `correlation_id`, `master_order_ids`, `sub_order_ids`.
pub type ResultTuple = (i32, Vec<String>, Vec<String>, Vec<String>);

/// A struct for the `mobile-service-layer-checkout` searcher.
#[derive(Clone, Debug)]
pub struct MslCheckout {
    idx: EchoIndex,
}

impl Default for MslCheckout {
    fn default() -> Self {
        Self {
            idx: EchoIndex::MslCheckout,
        }
    }
}

impl Searcher for MslCheckout {
    type ResultTuple = ResultTuple;
    type InsertTuple = InsertTuple;

    fn build_request(
        &self,
        date_math: String,
        _correlation_ids: Option<Vec<String>>,
    ) -> RequestBody {
        let mut search: RequestBody = Default::default();
        search.set_size(Some(1000));
        search.set_source(Some(vec![
            "timestamp".to_string(),
            "correlationId".to_string(),
            "destinationPath".to_string(),
            "responseCode".to_string(),
            "messageDetail.User-Agent".to_string(),
            "messageDetail.draftOrder_masterOrderId".to_string(),
            "messageDetail.draftOrder_subOrders".to_string(),
            "messageDetail.profileId".to_string(),
            "messageDetail.problemArea".to_string(),
            "messageDetail.validationCode".to_string(),
            "messageDetail.internalMessage".to_string(),
        ]));

        let mut gte_timestamp: Timestamp = Default::default();
        // Now minus 15 minutes rounded down to the start of that minute.
        gte_timestamp.set_gte(Some(date_math));
        gte_timestamp.set_time_zone(Some("-04:00".to_string()));

        let mut range: Range = Default::default();
        range.set_timestamp(Some(gte_timestamp));

        let mut message_match = HashMap::new();
        message_match.insert("message".to_string(), "http-inbound-request".to_string());

        let mut wildcard_match = HashMap::new();
        wildcard_match.insert(
            "destinationPath".to_string(),
            "POST /mobilecheckout/api/*/order/*".to_string(),
        );

        let mut match_param: Param = Default::default();
        match_param.set_matcher(Some(message_match));

        let mut range_param: Param = Default::default();
        range_param.set_range(Some(range));

        let mut wildcard_param: Param = Default::default();
        wildcard_param.set_wildcard(Some(wildcard_match));

        let mut bool_query: Bool = Default::default();
        bool_query.set_must(Some(vec![match_param, range_param, wildcard_param]));

        let mut filter: Filter = Default::default();
        filter.set_bool_query(Some(bool_query));

        let mut constant_score: ConstantScore = Default::default();
        constant_score.set_filter(Some(filter));

        let mut query: Query = Default::default();
        query.set_constant_score(Some(constant_score));

        search.set_query(Some(query));

        search
    }

    fn search(
        &self,
        client: &mut Client,
        conn: &mut Connection,
        config: &Config,
        search_config: &SearchConfig,
        request: &RequestBody,
    ) -> Result<Self::ResultTuple> {
        let mut count = 0;
        let mut correlations_ids = Vec::new();
        let mut master_order_ids = Vec::new();
        let mut sub_order_ids = Vec::new();
        let mut scrolling = false;
        let mut scroll_id = None;

        loop {
            let matches: Matches<Source> = if scrolling {
                client.set_url(&atter::scroll_url(&config, search_config.env()))?;
                let scroll_str = serde_json::to_string(&Scroll {
                    scroll: "1m".to_string(),
                    scroll_id: utils::clone_or(&scroll_id),
                })?;
                try_trace!(search_config.logs().stdout(), "Scroll: {}", scroll_str);
                let json_str = libeko::scroll(client as &mut EchoSearchClient, &scroll_str)?;
                serde_json::from_str(&json_str)?
            } else {
                client.set_url(&atter::echo_url(
                    &config,
                    search_config.env(),
                    &EchoIndex::MslCheckout,
                    true,
                ))?;
                let search_str = serde_json::to_string(request)?;
                try_trace!(search_config.logs().stdout(), "Search: {}", search_str);
                let json_str = libeko::search(client as &mut EchoSearchClient, &search_str)?;
                serde_json::from_str(&json_str)?
            };

            let hits = matches.hits();
            scroll_id = matches.scroll_id().clone();
            try_trace!(search_config.logs().stdout(), "Hits: {}", hits.total());
            try_trace!(
                search_config.logs().stdout(),
                "Scroll ID: {}",
                utils::clone_or(matches.scroll_id())
            );
            let documents = hits.hits();

            if documents.is_empty() {
                break;
            }

            let mut tuples = Vec::new();

            for document in documents {
                // Grab the Echo id.
                let echo_id = document.id();

                // Grab the _source data.
                let source = document.source();
                let correlation_id = source.correlation_id();
                let timestamp: i64 = source.timestamp().parse::<i64>()?;
                let destination_path = source.destination_path();
                let response_code = source.response_code();

                // Grab the message detail data
                let message_detail = source.message_detail();
                let user_agent = utils::clone_or(message_detail.user_agent());
                let profile_id = utils::clone_or(message_detail.profile_id());
                let master_order_id = utils::clone_or(message_detail.master_order_id());
                let suborders_info = utils::clone_or(message_detail.sub_orders());
                let problem_area = if let Some(problem_area) = message_detail.problem_area() {
                    problem_area
                } else {
                    ""
                };
                let validation_code =
                    if let Some(validation_code) = message_detail.validation_code() {
                        validation_code
                    } else {
                        ""
                    };
                let internal_message =
                    if let Some(internal_message) = message_detail.internal_message() {
                        internal_message
                    } else {
                        ""
                    };

                // Parse the version and endpoint out of the destination_path
                let (version_str, endpoint) = if CREATE.is_match(destination_path) {
                    if let Some(caps) = CREATE.captures(destination_path) {
                        (caps.get(1).map_or("0", |vstr| vstr.as_str()), "CREATE")
                    } else {
                        ("0", "UNKNOWN")
                    }
                } else if RELEASE.is_match(destination_path) {
                    if let Some(caps) = RELEASE.captures(destination_path) {
                        (caps.get(1).map_or("0", |vstr| vstr.as_str()), "RELEASE")
                    } else {
                        ("0", "UNKNOWN")
                    }
                } else {
                    ("0", "UNKNOWN")
                };

                let version = version_str.parse::<u8>()?;

                correlations_ids.push(correlation_id.clone());
                master_order_ids.push(master_order_id.clone());

                let mut suborders_split = suborders_info.split(',');
                let suborders_parts = suborders_split.collect::<Vec<&str>>();

                for suborder_part in suborders_parts {
                    let mut suborder_split = suborder_part.split(':');
                    let parts = suborder_split.collect::<Vec<&str>>();

                    let (fulfillment, sub_order_id) = if parts.len() == 2 {
                        (parts[0], parts[1])
                    } else {
                        ("0", "0")
                    };

                    sub_order_ids.push(sub_order_id.to_string());

                    tuples.push((
                        echo_id.clone(),
                        correlation_id.clone(),
                        profile_id.clone(),
                        timestamp,
                        version,
                        endpoint.to_string(),
                        *response_code,
                        fulfillment.to_string(),
                        master_order_id.clone(),
                        sub_order_id.to_string(),
                        user_agent.clone(),
                        problem_area.to_string(),
                        validation_code.to_string(),
                        internal_message.to_string(),
                    ));
                }
            }

            count += self.insert(conn, tuples)?;

            if scroll_id.is_none() {
                break;
            } else if let Some(ref scroll_id) = scroll_id {
                if scroll_id.is_empty() {
                    break;
                } else {
                    scrolling = true;
                }
            }
        }
        Ok((count, correlations_ids, master_order_ids, sub_order_ids))
    }

    fn insert(&self, conn: &mut Connection, tuples: Vec<Self::InsertTuple>) -> Result<i32> {
        let tx = conn.transaction()?;
        let mut count = 0;
        {
            let insert_sql = format!(
                "INSERT INTO {} (
                    echo_id,
                    correlation_id,
                    profile_id,
                    timestamp,
                    version,
                    endpoint,
                    response_code,
                    fulfillment,
                    master_order_id,
                    sub_order_id,
                    user_agent,
                    problem_area,
                    validation_code,
                    internal_message
                ) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14)",
                self.idx.to_string().replace("-", "_")
            );

            let mut stmt = tx.prepare(&insert_sql)?;
            for (
                echo_id,
                correlation_id,
                profile_id,
                timestamp,
                version,
                endpoint,
                response_code,
                fulfillment,
                master_order_id,
                suborders,
                user_agent,
                problem_area,
                validation_code,
                internal_message,
            ) in tuples
            {
                count += stmt.execute(&[
                    &echo_id,
                    &correlation_id,
                    &profile_id,
                    &timestamp,
                    &version,
                    &endpoint,
                    &response_code,
                    &fulfillment,
                    &master_order_id,
                    &suborders,
                    &user_agent,
                    &problem_area,
                    &validation_code,
                    &internal_message,
                ])?;
            }
        }
        tx.commit()?;
        Ok(count)
    }
}

/// The hit source data.
#[derive(Debug, Deserialize, Getters)]
#[serde(rename_all = "camelCase")]
pub struct Source {
    /// Correlation ID
    #[get = "pub"]
    correlation_id: String,
    /// Message Detail
    #[get = "pub"]
    message_detail: MessageDetail,
    /// Timestamp
    #[get = "pub"]
    timestamp: String,
    /// Response Code
    #[get = "pub"]
    response_code: u16,
    /// Destination Path
    #[get = "pub"]
    destination_path: String,
}

/// The message detail.
#[derive(Debug, Deserialize, Getters)]
#[serde(rename_all = "camelCase")]
pub struct MessageDetail {
    /// User-Agent
    #[get = "pub"]
    #[serde(rename = "User-Agent")]
    user_agent: Option<String>,
    /// Account ID (Profile ID)
    #[get = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    profile_id: Option<String>,
    /// Master Order ID
    #[get = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "draftOrder_masterOrderId")]
    master_order_id: Option<String>,
    /// SubOrders
    #[get = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "draftOrder_subOrders")]
    sub_orders: Option<String>,
    /// Problem Area
    #[get = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    problem_area: Option<String>,
    /// Validation Code
    #[get = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    validation_code: Option<String>,
    /// Internal Message
    #[get = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    internal_message: Option<String>,
}
