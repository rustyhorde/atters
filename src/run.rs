// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `atters` runtime
use atter::{self, RuntimeEnvironment};
use clap::{App, Arg};
use error::{ErrorKind, Result};
use futures::sync::mpsc;
use futures::{Future, Stream};
use log::Logs;
use sender::{self, SearchConfig};
use slog::Level;
use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::File;
use std::io::BufReader;
use std::net::SocketAddr;
use std::rc::Rc;
use std::thread;
use std::time::Duration;
use tokio_core::net::TcpListener;
use tokio_core::reactor::Core;
use tokio_io::io::write_all;
use tokio_io::AsyncRead;

/// CLI Runtime
pub fn run() -> Result<i32> {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Monitor the Mobile Checkout related Echo indices to identify auth'd orders with no fulfillment.")
        .arg(
            Arg::with_name("env")
                .short("e")
                .long("env")
                .takes_value(true)
                .required(true)
                .default_value("dev")
                .possible_values(&["prod", "stage", "test", "dev"])
                .help("The environment to run against")
                .long_help(
"The environment to run against.
Currently, this is used to determine the base Echo URL and generate the Echo index values.
For example, the base checkout index is 'mobile-service-layer-checkout'.  This environment
is appended to form the actual index of 'mobile-service-layer-checkout-<env>'.
"
                ),
        )
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .takes_value(true)
                .required(true)
                .default_value(".atters.toml")
                .help("The config file to use")
                .long_help(
"The config file to use.
The TOML config file contains the base sqlite database path, the table definitions, and
environment specific settings.  See the comments in the .atters.toml file included for
more detailed descriptions.
"
                ),
        )
        .arg(
            Arg::with_name("address")
                .short("a")
                .long("address")
                .takes_value(true)
                .required(true)
                .default_value("127.0.0.1:8888")
                .help("The listen address for this server")
                .long_help(
"The listen address for this server.
atterc can be used to connect to this server and record output.
"
                ),
        )
        .arg(
            Arg::with_name("date")
                .short("d")
                .long("date")
                .takes_value(true)
                .default_value("now-15m/m")
                .help("A date math string to use on the initial search query")
                .long_help(
"A date math string to use on the initial search query.
For example, now-15m/m with use greater than 15 minutes ago
rounded to the nearest minute.
"
                ),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .multiple(true)
                .help("Set the output verbosity level (more v's = more verbose)"),
        )
        .arg(
            Arg::with_name("quiet")
                .short("q")
                .long("quiet")
                .multiple(true)
                .conflicts_with("verbose")
                .help("Restrict output.  (more q's = more quiet"),
        )
        .arg(
            Arg::with_name("username")
                .short("u")
                .long("username")
                .required(true)
                .takes_value(true)
                .help("Your EUID"),
        )
        .arg(
            Arg::with_name("password")
                .short("p")
                .long("password")
                .required(true)
                .takes_value(true)
                .help("Your password"),
        )
        .get_matches();

    // Setup the runtime environment
    let env: RuntimeEnvironment = TryFrom::try_from(matches
        .value_of("env")
        .ok_or_else(|| ErrorKind::InvalidEnv)?)?;

    // Setup the logging level (info by default)
    let mut level = match matches.occurrences_of("verbose") {
        0 => Level::Info,
        1 => Level::Debug,
        2 | _ => Level::Trace,
    };

    level = match matches.occurrences_of("quiet") {
        0 => level,
        1 => Level::Warning,
        2 => Level::Error,
        3 | _ => Level::Critical,
    };

    let mut logs: Logs = Default::default();
    logs.set_stdout_level(level);

    // Logging clones for server, monitor threads, receiver, and config.
    let server_logs = logs.clone();
    let sender_logs = logs.clone();
    let receiver_logs = logs.clone();
    let thread_logs = logs.clone();
    let core_logs = logs.clone();

    try_trace!(logs.stdout(), "Logging configured!");

    // Read the config file.
    let config_file = File::open(matches.value_of("config").ok_or("invalid config file")?)?;
    let mut reader = BufReader::new(config_file);
    let config = atter::read_toml(&mut reader)?;

    try_trace!(logs.stdout(), "Configuration TOML parsed!");

    let addr = matches
        .value_of("address")
        .ok_or("invalid address")?
        .parse::<SocketAddr>()?;
    let mut core = Core::new()?;
    let remote_handle = core.remote();
    let handle = core.handle();
    let socket = TcpListener::bind(&addr, &handle)?;

    try_trace!(logs.stdout(), "Listening for connections"; "addr" => format!("{}", addr));

    // This is a single-threaded server, so we can just use Rc and RefCell to
    // store the map of all connections we know about.
    let connections = Rc::new(RefCell::new(HashMap::new()));

    // Clone some conns for the worker and for the server to reference.
    let rx_cons = Rc::clone(&connections);
    let srv_cons = Rc::clone(&connections);

    let srv = socket.incoming().for_each(move |(stream, addr)| {
        try_trace!(server_logs.stdout(), "Connection opened"; "addr" => format!("{}", addr));
        // We currently don't accept input from the clients, so only grabbing the writer.
        let (_r, writer) = stream.split();

        // Create a channel for our stream, which other sockets will use to
        // send us messages. Then register our address with the stream to send
        // data to us.
        let (tx, rx) = mpsc::unbounded();
        connections.borrow_mut().insert(addr, tx);

        // Whenever we receive a string on the Receiver, we write it to
        // `WriteHalf<TcpStream>`.
        let writer_logs = logs.clone();
        let socket_writer = rx.fold(writer, move |writer, msg: Vec<u8>| {
            try_trace!(writer_logs.stdout(), "Sending bincoded monitor"; "addr" => format!("{}", addr));
            let writer_buf_tuple = write_all(writer, msg);
            let writer = writer_buf_tuple.map(|(writer, _)| writer);
            writer.map_err(|_| ())
        });

        // Make the socket write future into a future that can be spawned.
        let socket_writer = socket_writer.map(|_| ());

        let connections = Rc::clone(&srv_cons);
        let spawn_logs = server_logs.clone();
        handle.spawn(socket_writer.then(move |_| {
            try_trace!(spawn_logs.stdout(), "Closing connection"; "addr" => format!("{}", addr));
            connections.borrow_mut().remove(&addr);
            Ok(())
        }));

        Ok(())
    });

    let srv = srv.map_err(|_| ());

    // The tx gets cloned into monitor threads for sending messages.
    // The rx send received messages to connected clients.
    let (tx, rx) = mpsc::unbounded();

    // Setup the search thread.
    let username = matches
        .value_of("username")
        .ok_or_else(|| ErrorKind::InvalidUsername)?;
    let password = matches
        .value_of("password")
        .ok_or_else(|| ErrorKind::InvalidPassword)?;
    let date_math = matches.value_of("date");
    let search_config = SearchConfig::new(
        env,
        &config,
        username,
        password,
        date_math.map(|s| s.to_string()),
        tx,
        sender_logs,
        remote_handle,
    );

    thread::spawn(move || loop {
        if let Err(e) = sender::run(&config, &search_config) {
            try_error!(thread_logs.stderr(), "Error starting thread: {}", e);
            try_error!(thread_logs.stderr(), "Restarting sender in 30s");
            thread::sleep(Duration::new(30, 0));
        }
    });

    // This is where we send messages from the senders off to any connected clients.
    let rx_fut = rx.for_each(|message_result| {
        match message_result {
            Ok(message) => {
                let mut conns = rx_cons.borrow_mut();
                for tx in conns.iter_mut().map(|(_, v)| v) {
                    if tx.unbounded_send(message.clone()).is_err() {
                        try_error!(receiver_logs.stderr(), "Error sending message");
                    }
                }
            }
            Err(()) => try_error!(receiver_logs.stderr(), "Error"),
        }
        Ok(())
    });

    // Join the server and monitor futures.
    let both = rx_fut.join(srv);

    try_info!(core_logs.stdout(), "Starting {}...", env!("CARGO_PKG_NAME"));

    // Run the monitors and the server.
    core.run(both).expect("Failed to run event loop");

    Ok(0)
}
