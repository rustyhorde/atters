// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch Time Units
use serde::{Serialize, Serializer};

/// Whenever durations need to be specified, e.g. for a timeout parameter, the duration must specify the unit, like 2d for 2 days.
#[derive(Debug, Deserialize)]
pub enum TimeUnits {
    /// Days
    Days(usize),
    /// Hours
    Hours(usize),
    /// Minutes
    Minutes(usize),
    /// Seconds
    Seconds(usize),
    /// Milliseconds
    Milliseconds(usize),
    /// Microseconds
    Microseconds(usize),
    /// Nanoseconds
    Nanoseconds(usize),
}

impl Serialize for TimeUnits {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let time_unit_str = match *self {
            TimeUnits::Days(u) => format!("{}d", u),
            TimeUnits::Hours(u) => format!("{}h", u),
            TimeUnits::Minutes(u) => format!("{}m", u),
            TimeUnits::Seconds(u) => format!("{}s", u),
            TimeUnits::Milliseconds(u) => format!("{}ms", u),
            TimeUnits::Microseconds(u) => format!("{}micros", u),
            TimeUnits::Nanoseconds(u) => format!("{}nanos", u),
        };
        serializer.serialize_str(&time_unit_str)
    }
}

#[cfg(test)]
mod test {
    use super::TimeUnits;
    use serde_json;

    const JSON_SER_FAIL: &str = "Failed to serialize TimeUnits";

    fn check_ser(time_units: &TimeUnits, expected: &str) {
        assert_eq!(
            expected,
            serde_json::to_string(time_units).expect(JSON_SER_FAIL)
        );
    }

    #[test]
    fn days() {
        check_ser(&TimeUnits::Days(2), "\"2d\"");
    }

    #[test]
    fn hours() {
        check_ser(&TimeUnits::Hours(4), "\"4h\"");
    }

    #[test]
    fn minutes() {
        check_ser(&TimeUnits::Minutes(3), "\"3m\"");
    }

    #[test]
    fn seconds() {
        check_ser(&TimeUnits::Seconds(20), "\"20s\"");
    }

    #[test]
    fn milliseconds() {
        check_ser(&TimeUnits::Milliseconds(250), "\"250ms\"");
    }

    #[test]
    fn microseconds() {
        check_ser(&TimeUnits::Microseconds(100), "\"100micros\"");
    }

    #[test]
    fn nanoseconds() {
        check_ser(&TimeUnits::Nanoseconds(5000), "\"5000nanos\"");
    }
}
