// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch search response shards JSON.
use serde_json;
use std::fmt;

/// Shard information in search result
#[derive(Debug, Deserialize, Serialize)]
pub struct Shards {
    /// Total shards used.
    total: usize,
    /// Successful shards.
    successful: usize,
    /// Skipped shards.
    skipped: usize,
    /// Failed shards.
    failed: usize,
}

impl fmt::Display for Shards {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match serde_json::to_string(self) {
            Ok(json_str) => write!(f, "{}", json_str),
            Err(e) => write!(f, "{}", e),
        }
    }
}
