// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Search Type
use serde::{Serialize, Serializer};

/// There are different execution paths that can be done when executing a distributed search.
/// The distributed search operation needs to be scattered to all the relevant shards and then all the results are gathered back.
/// When doing scatter/gather type execution, there are several ways to do that, specifically with search engines.
#[derive(Debug, Deserialize)]
pub enum Type {
    /// Same as "Query Then Fetch", except for an initial scatter phase which goes and computes the distributed term frequencies for more accurate scoring.
    DfsQueryThenFetch,
    /// The request is processed in two phases. In the first phase, the query is forwarded to all involved shards.
    /// Each shard executes the search and generates a sorted list of results, local to that shard.
    /// Each shard returns just enough information to the coordinating node to allow it merge and re-sort the shard level results into a globally sorted set of results, of maximum length size.
    ///
    /// During the second phase, the coordinating node requests the document content (and highlighted snippets, if any) from only the relevant shards.
    ///
    /// This is the default setting, if you do not specify a `search_type` in your request.
    QueryThenFetch,
}

impl Serialize for Type {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let type_str = match *self {
            Type::DfsQueryThenFetch => "dfs_query_then_fetch",
            Type::QueryThenFetch => "query_then_fetch",
        };
        serializer.serialize_str(type_str)
    }
}

#[cfg(test)]
mod test {
    use super::Type;
    use serde_json;

    #[test]
    fn dfs_query_then_fetch() {
        assert_eq!(
            "\"dfs_query_then_fetch\"",
            serde_json::to_string(&Type::DfsQueryThenFetch).expect("failed to serialize Type")
        );
    }

    #[test]
    fn query_then_fetch() {
        assert_eq!(
            "\"query_then_fetch\"",
            serde_json::to_string(&Type::QueryThenFetch).expect("failed to serialize Type")
        );
    }
}
