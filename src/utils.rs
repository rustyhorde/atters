// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `atters` utils

pub fn clone_or(val: &Option<String>) -> String {
    if let Some(s) = val {
        s.clone()
    } else {
        "".to_string()
    }
}
