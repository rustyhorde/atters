// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! proxy-banner source definition.
use atter::{self, Config, EchoIndex};
use error::Result;
use libeko::{self, Client, EchoSearchClient};
use regex::Regex;
use rusqlite::Connection;
use search::{Bool, ConstantScore, Filter, Matches, Param, Query, Range, RequestBody, Scroll,
             Searcher, Timestamp};
use sender::SearchConfig;
use serde_json;
use std::collections::HashMap;
use utils;

lazy_static! {
    static ref ORDER_DETAILS: Regex =
        Regex::new(r"^/order-details/(\d*)").expect("unable to create ORDER_DETAILS regex");
}

/// `echo_id`, `correlation-id`, `timestamp`, `order_id`, `response_code`, `payload_size`, `bytes_sent`, `duration`.
pub type InsertTuple = (String, String, i64, String, u16, i64, i64, i64);

/// A struct for the `proxy-banner` searcher.
#[derive(Clone, Debug)]
pub struct ProxyBanner {
    idx: EchoIndex,
}

impl Default for ProxyBanner {
    fn default() -> Self {
        Self {
            idx: EchoIndex::ProxyBanner,
        }
    }
}

impl Searcher for ProxyBanner {
    type ResultTuple = (i32, ());
    type InsertTuple = InsertTuple;

    fn build_request(&self, date_math: String, sub_order_ids: Option<Vec<String>>) -> RequestBody {
        let mut search: RequestBody = Default::default();
        search.set_size(Some(1000));
        search.set_source(Some(vec![
            "correlationId".to_string(),
            "timestamp".to_string(),
            "destinationPath".to_string(),
            "responseCode".to_string(),
            "payloadSize".to_string(),
            "messageDetail.bytesSent".to_string(),
            "duration".to_string(),
        ]));

        let mut gte_timestamp: Timestamp = Default::default();
        // Now minus 15 minutes rounded down to the start of that minute.
        gte_timestamp.set_gte(Some(date_math));
        gte_timestamp.set_time_zone(Some("-04:00".to_string()));

        let mut range: Range = Default::default();
        range.set_timestamp(Some(gte_timestamp));

        let mut request_method_match = HashMap::new();
        request_method_match.insert(
            "messageDetail.requestMethod".to_string(),
            "POST".to_string(),
        );

        let terms = sub_order_ids
            .unwrap_or_else(Vec::new)
            .iter()
            .map(|x| format!("/order-details/{}", x))
            .collect::<Vec<String>>();
        let mut terms_match = HashMap::new();
        terms_match.insert("destinationPath".to_string(), terms);

        let mut range_param: Param = Default::default();
        range_param.set_range(Some(range));

        let mut request_method_param: Param = Default::default();
        request_method_param.set_term(Some(request_method_match));

        let mut terms_param: Param = Default::default();
        terms_param.set_terms(Some(terms_match));

        let mut bool_query: Bool = Default::default();
        bool_query.set_must(Some(vec![terms_param, request_method_param, range_param]));

        let mut filter: Filter = Default::default();
        filter.set_bool_query(Some(bool_query));

        let mut constant_score: ConstantScore = Default::default();
        constant_score.set_filter(Some(filter));

        let mut query: Query = Default::default();
        query.set_constant_score(Some(constant_score));

        search.set_query(Some(query));

        search
    }

    fn search(
        &self,
        client: &mut Client,
        conn: &mut Connection,
        config: &Config,
        search_config: &SearchConfig,
        request: &RequestBody,
    ) -> Result<Self::ResultTuple> {
        let mut count = 0;
        let mut scrolling = false;
        let mut scroll_id = None;

        loop {
            let matches: Matches<Source> = if scrolling {
                client.set_url(&atter::scroll_url(&config, search_config.env()))?;
                let scroll_str = serde_json::to_string(&Scroll {
                    scroll: "1m".to_string(),
                    scroll_id: utils::clone_or(&scroll_id),
                })?;
                try_trace!(search_config.logs().stdout(), "Scroll: {}", scroll_str);
                let json_str = libeko::scroll(client as &mut EchoSearchClient, &scroll_str)?;
                serde_json::from_str(&json_str)?
            } else {
                client.set_url(&atter::echo_url(
                    &config,
                    search_config.env(),
                    &EchoIndex::ProxyBanner,
                    true,
                ))?;
                let search_str = serde_json::to_string(request)?;
                try_trace!(search_config.logs().stdout(), "Search: {}", search_str);
                let json_str = libeko::search(client as &mut EchoSearchClient, &search_str)?;
                try_trace!(search_config.logs().stdout(), "Result: {}", json_str);
                serde_json::from_str(&json_str)?
            };

            let hits = matches.hits();
            scroll_id = matches.scroll_id().clone();
            try_trace!(search_config.logs().stdout(), "Hits: {}", hits.total());
            try_trace!(
                search_config.logs().stdout(),
                "Scroll ID: {}",
                utils::clone_or(matches.scroll_id())
            );
            let documents = hits.hits();

            if documents.is_empty() {
                break;
            }

            let mut tuples = Vec::new();

            for document in documents {
                let echo_id = document.id();
                let source = document.source();
                let timestamp = source.timestamp();
                let correlation_id = source.correlation_id();
                let destination_path = source.destination_path();
                let response_code = source.response_code();
                let payload_size = source.payload_size();
                let duration = source.duration();
                let message_detail = source.message_detail();
                let bytes_sent = message_detail.bytes_sent();

                let order_id = if ORDER_DETAILS.is_match(destination_path) {
                    if let Some(caps) = ORDER_DETAILS.captures(destination_path) {
                        caps.get(1).map_or("0", |oid| oid.as_str())
                    } else {
                        "0"
                    }
                } else {
                    "0"
                };

                tuples.push((
                    echo_id.clone(),
                    correlation_id.clone(),
                    *timestamp,
                    order_id.to_string(),
                    *response_code,
                    *payload_size,
                    *bytes_sent,
                    *duration,
                ));
            }

            if !tuples.is_empty() {
                count += self.insert(conn, tuples)?;
            }

            if scroll_id.is_none() {
                break;
            } else if let Some(ref scroll_id) = scroll_id {
                if scroll_id.is_empty() {
                    break;
                } else {
                    scrolling = true;
                }
            }
        }
        Ok((count, ()))
    }

    fn insert(&self, conn: &mut Connection, tuples: Vec<Self::InsertTuple>) -> Result<i32> {
        let tx = conn.transaction()?;
        let mut count = 0;
        {
            let insert_sql = format!(
                "INSERT INTO {} (
                    echo_id,
                    correlation_id,
                    timestamp,
                    order_id,
                    response_code,
                    payload_size,
                    bytes_sent,
                    duration
                ) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)",
                self.idx.to_string().replace("-", "_")
            );
            let mut stmt = tx.prepare(&insert_sql)?;
            for (
                echo_id,
                correlation_id,
                timestamp,
                order_id,
                response_code,
                payload_size,
                bytes_sent,
                duration,
            ) in tuples
            {
                count += stmt.execute(&[
                    &echo_id,
                    &correlation_id,
                    &timestamp,
                    &order_id,
                    &response_code,
                    &payload_size,
                    &bytes_sent,
                    &duration,
                ])?;
            }
        }
        tx.commit()?;
        Ok(count)
    }
}

/// The hit source data.
#[derive(Debug, Deserialize, Getters)]
#[serde(rename_all = "camelCase")]
pub struct Source {
    /// Correlation ID
    #[get = "pub"]
    correlation_id: String,
    /// Timestamp
    #[get = "pub"]
    timestamp: i64,
    /// Destination Path
    #[get = "pub"]
    destination_path: String,
    /// Response Code
    #[get = "pub"]
    response_code: u16,
    /// Payload size
    #[get = "pub"]
    payload_size: i64,
    /// Message detail block.
    #[get = "pub"]
    message_detail: MessageDetail,
    /// Duration
    #[get = "pub"]
    duration: i64,
}

/// Message detail block
#[derive(Debug, Deserialize, Getters)]
#[serde(rename_all = "camelCase")]
pub struct MessageDetail {
    /// Bytes sent.
    #[get = "pub"]
    bytes_sent: i64,
}
