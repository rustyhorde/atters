// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Request Body Search Structs
use search::request::Query;
#[cfg(test)]
use search::request::{TimeUnits, Type};
use serde_json;
use std::fmt;

/// The search request can be executed with a search DSL, which includes the Query DSL, within its body.
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct RequestBody {
    /// A search timeout, bounding the search request to be executed within the specified time value and bail with the hits accumulated up to that point when expired. Defaults to no timeout.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg(test)]
    timeout: Option<TimeUnits>,
    /// To retrieve hits from a certain offset. Defaults to 0.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg(test)]
    from: Option<usize>,
    /// The number of hits to return. Defaults to 10. If you do not care about getting some hits back but only about the number of matches and/or aggregations, setting the value to 0 will help performance.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    size: Option<usize>,
    /// The type of the search operation to perform. Defaults to `query_then_fetch`.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg(test)]
    search_type: Option<Type>,
    /// Set to `true` or `false` to enable or disable the caching of search results for requests where `size` is 0, i.e. aggregations and suggestions.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg(test)]
    request_cache: Option<bool>,
    /// The maximum number of documents to collect for each shard, upon reaching which the query execution will terminate early.
    /// If set, the response will have a boolean field `terminated_early` to indicate whether the query execution has actually terminated early. Defaults to no terminate_after.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg(test)]
    terminate_after: Option<usize>,
    /// The number of shard results that should be reduced at once on the coordinating node.
    /// This value should be used as a protection mechanism to reduce the memory overhead per search request if the potential number of shards in the request can be large.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg(test)]
    batched_reduce_size: Option<usize>,
    /// The query context.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    query: Option<Query>,
    /// The json data to extract.
    #[serde(rename = "_source")]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[set = "pub"]
    source: Option<Vec<String>>,
}

impl fmt::Display for RequestBody {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match serde_json::to_string(self) {
            Ok(json_str) => write!(f, "{}", json_str),
            Err(e) => write!(f, "{}", e),
        }
    }
}

#[cfg(test)]
mod test {
    use super::RequestBody;
    use search::request::{Query, TimeUnits, Type};
    use serde_json;

    const JSON_SER_FAIL: &str = "Failed to serialize RequestBody";

    fn default_request() -> RequestBody {
        Default::default()
    }

    fn check_ser(request_body: &RequestBody, expected: &str) {
        assert_eq!(
            expected,
            serde_json::to_string(request_body).expect(JSON_SER_FAIL)
        );
    }

    #[test]
    fn only_timeout() {
        let mut request_body = default_request();
        request_body.set_timeout(Some(TimeUnits::Seconds(10)));
        check_ser(&request_body, "{\"timeout\":\"10s\"}");
    }

    #[test]
    fn only_from() {
        let mut request_body = default_request();
        request_body.set_from(Some(100));
        check_ser(&request_body, "{\"from\":100}");
    }

    #[test]
    fn only_size() {
        let mut request_body = default_request();
        request_body.set_size(Some(1000));
        check_ser(&request_body, "{\"size\":1000}");
    }

    #[test]
    fn only_search_type() {
        let mut request_body = default_request();
        request_body.set_search_type(Some(Type::DfsQueryThenFetch));
        check_ser(&request_body, "{\"search_type\":\"dfs_query_then_fetch\"}");
    }

    #[test]
    fn only_request_cache() {
        let mut request_body = default_request();
        request_body.set_request_cache(Some(false));
        check_ser(&request_body, "{\"request_cache\":false}");
    }

    #[test]
    fn only_terminate_after() {
        let mut request_body = default_request();
        request_body.set_terminate_after(Some(100));
        check_ser(&request_body, "{\"terminate_after\":100}");
    }

    #[test]
    fn only_batched_reduce_size() {
        let mut request_body = default_request();
        request_body.set_batched_reduce_size(Some(2));
        check_ser(&request_body, "{\"batched_reduce_size\":2}");
    }

    #[test]
    fn full() {
        let mut request_body = default_request();
        request_body.set_timeout(Some(TimeUnits::Seconds(10)));
        request_body.set_size(Some(1000));
        request_body.set_search_type(Some(Type::DfsQueryThenFetch));
        request_body.set_request_cache(Some(false));
        request_body.set_terminate_after(Some(100));
        request_body.set_batched_reduce_size(Some(2));
        request_body.set_source(Some(vec!["messageDetail.orderId".to_string()]));

        let mut query: Query = Default::default();
        query.set_match_all(Some(Default::default()));

        request_body.set_query(Some(query));

        check_ser(&request_body, "{\"timeout\":\"10s\",\"size\":1000,\"search_type\":\"dfs_query_then_fetch\",\"request_cache\":false,\"terminate_after\":100,\"batched_reduce_size\":2,\"query\":{\"match_all\":{}},\"_source\":[\"messageDetail.orderId\"]}");
    }
}
