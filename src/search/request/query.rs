// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch Query Structs
use std::collections::HashMap;

/// The query context.
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct Query {
    /// A constant score compound query.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[set = "pub"]
    constant_score: Option<ConstantScore>,
    /// A match all query.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[set = "pub"]
    #[cfg(test)]
    match_all: Option<MatchAll>,
}

#[derive(Default, Deserialize, Serialize, Setters)]
#[cfg(test)]
pub struct MatchAll {}

/// A query which wraps another query, but executes it in filter context. All matching documents are given the same “constant” `_score`.
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct ConstantScore {
    /// A filter to apply to the results.
    #[set = "pub"]
    filter: Option<Filter>,
}

/// A search filter.
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct Filter {
    /// Boolean search criteria.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "bool")]
    #[set = "pub"]
    bool_query: Option<Bool>,
}

/// Boolean search criteria.
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct Bool {
    /// The results must match all the supplied params.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[set = "pub"]
    must: Option<Vec<Param>>,
}

/// A search parameter.
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct Param {
    /// A match parameter.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "match")]
    matcher: Option<HashMap<String, String>>,
    /// A range option.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    range: Option<Range>,
    /// A term parameter.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    term: Option<HashMap<String, String>>,
    /// A terms parameter.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    terms: Option<HashMap<String, Vec<String>>>,
    /// A wildcard parameter.
    #[set = "pub"]
    #[serde(skip_serializing_if = "Option::is_none")]
    wildcard: Option<HashMap<String, String>>,
}

/// Range parameter
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct Range {
    /// Does the document fall within the given time range.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[set = "pub"]
    timestamp: Option<Timestamp>,
}

/// Timestamp parameter
#[derive(Default, Deserialize, Serialize, Setters)]
pub struct Timestamp {
    /// Is the timestamp greater than or equal to the given value.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[set = "pub"]
    gte: Option<String>,
    /// Set the timezone offset for the given timestamp.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[set = "pub"]
    time_zone: Option<String>,
}
