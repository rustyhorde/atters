// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch search response matches top level JSON.
use search::response::{Hits, Shards};
use serde::Serialize;
use serde_json;
use std::fmt;

/// Elasticsearch Matches
#[derive(Debug, Deserialize, Getters, Serialize)]
pub struct Matches<T> {
    /// How long did the search take?
    took: usize,
    /// Did the search time out?
    timed_out: bool,
    /// Scroll ID to use with scroll api.
    #[get = "pub"]
    #[serde(rename = "_scroll_id")]
    #[serde(skip_serializing_if = "Option::is_none")]
    scroll_id: Option<String>,
    /// Shards information.
    #[serde(rename = "_shards")]
    shards: Shards,
    /// Document hits.
    #[get = "pub"]
    hits: Hits<T>,
}

impl<T> fmt::Display for Matches<T>
where
    T: Serialize,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match serde_json::to_string(self) {
            Ok(json_str) => write!(f, "{}", json_str),
            Err(e) => write!(f, "{}", e),
        }
    }
}
