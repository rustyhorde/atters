// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! proxy-api source definition.
use atter::{self, Config, EchoIndex};
use error::Result;
use libeko::{self, Client, EchoSearchClient};
use rusqlite::Connection;
use search::proxyapi::{InsertTuple, Source};
use search::{Bool, ConstantScore, Filter, Matches, Param, Query, Range, RequestBody, Scroll,
             Searcher, Timestamp};
use sender::SearchConfig;
use serde_json;
use std::collections::HashMap;
use utils;

/// A struct for the `proxy-api` by `correlation_id` searcher.
#[derive(Clone, Debug)]
pub struct ProxyApiCorrId {
    idx: EchoIndex,
}

impl Default for ProxyApiCorrId {
    fn default() -> Self {
        Self {
            idx: EchoIndex::ProxyApi,
        }
    }
}

impl Searcher for ProxyApiCorrId {
    type ResultTuple = (i32, ());
    type InsertTuple = InsertTuple;

    fn build_request(
        &self,
        date_math: String,
        correlation_ids: Option<Vec<String>>,
    ) -> RequestBody {
        let mut search: RequestBody = Default::default();
        search.set_size(Some(1000));
        search.set_source(Some(vec![
            "correlationId".to_string(),
            "timestamp".to_string(),
            "destinationPath".to_string(),
            "destinationHostName".to_string(),
            "responseCode".to_string(),
            "payloadSize".to_string(),
            "messageDetail.bytesSent".to_string(),
            "duration".to_string(),
        ]));

        let mut gte_timestamp: Timestamp = Default::default();
        // Now minus 15 minutes rounded down to the start of that minute.
        gte_timestamp.set_gte(Some(date_math));
        gte_timestamp.set_time_zone(Some("-04:00".to_string()));

        let mut range: Range = Default::default();
        range.set_timestamp(Some(gte_timestamp));

        let terms = correlation_ids.unwrap_or_else(Vec::new);
        let mut terms_match = HashMap::new();
        terms_match.insert("correlationId".to_string(), terms);

        let mut range_param: Param = Default::default();
        range_param.set_range(Some(range));

        let mut terms_param: Param = Default::default();
        terms_param.set_terms(Some(terms_match));

        let mut bool_query: Bool = Default::default();
        bool_query.set_must(Some(vec![terms_param, range_param]));

        let mut filter: Filter = Default::default();
        filter.set_bool_query(Some(bool_query));

        let mut constant_score: ConstantScore = Default::default();
        constant_score.set_filter(Some(filter));

        let mut query: Query = Default::default();
        query.set_constant_score(Some(constant_score));

        search.set_query(Some(query));

        search
    }

    fn search(
        &self,
        client: &mut Client,
        conn: &mut Connection,
        config: &Config,
        search_config: &SearchConfig,
        request: &RequestBody,
    ) -> Result<Self::ResultTuple> {
        let mut count = 0;
        let mut scrolling = false;
        let mut scroll_id = None;

        loop {
            let matches: Matches<Source> = if scrolling {
                client.set_url(&atter::scroll_url(&config, search_config.env()))?;
                let scroll_str = serde_json::to_string(&Scroll {
                    scroll: "1m".to_string(),
                    scroll_id: utils::clone_or(&scroll_id),
                })?;
                try_trace!(search_config.logs().stdout(), "Scroll: {}", scroll_str);
                let json_str = libeko::scroll(client as &mut EchoSearchClient, &scroll_str)?;
                serde_json::from_str(&json_str)?
            } else {
                client.set_url(&atter::echo_url(
                    &config,
                    search_config.env(),
                    &EchoIndex::ProxyApi,
                    true,
                ))?;
                let search_str = serde_json::to_string(request)?;
                try_trace!(search_config.logs().stdout(), "Search: {}", search_str);
                let json_str = libeko::search(client as &mut EchoSearchClient, &search_str)?;
                serde_json::from_str(&json_str)?
            };

            let hits = matches.hits();
            scroll_id = matches.scroll_id().clone();
            try_trace!(search_config.logs().stdout(), "Hits: {}", hits.total());
            try_trace!(
                search_config.logs().stdout(),
                "Scroll ID: {}",
                utils::clone_or(matches.scroll_id())
            );
            let documents = hits.hits();

            if documents.is_empty() {
                break;
            }

            let mut tuples = Vec::new();

            for document in documents {
                let echo_id = document.id();
                let source = document.source();
                let timestamp = source.timestamp();
                let destination_path = source.destination_path();
                let response_code = source.response_code();
                let correlation_id = source.correlation_id();
                let ip = source.ip();
                let payload_size = source.payload_size();
                let duration = source.duration();
                let message_detail = source.message_detail();
                let bytes_sent = message_detail.bytes_sent();

                tuples.push((
                    echo_id.clone(),
                    correlation_id.clone(),
                    *timestamp,
                    destination_path.clone(),
                    *response_code,
                    ip.clone(),
                    *payload_size,
                    *bytes_sent,
                    *duration,
                ));
            }

            count += self.insert(conn, tuples)?;

            if scroll_id.is_none() {
                break;
            } else if let Some(ref scroll_id) = scroll_id {
                if scroll_id.is_empty() {
                    break;
                } else {
                    scrolling = true;
                }
            }
        }
        Ok((count, ()))
    }

    fn insert(&self, conn: &mut Connection, tuples: Vec<Self::InsertTuple>) -> Result<i32> {
        let tx = conn.transaction()?;
        let mut count = 0;
        {
            let insert_sql = format!(
                "INSERT INTO {} (
                    echo_id,
                    correlation_id,
                    timestamp,
                    destination_path,
                    response_code,
                    ip,
                    payload_size,
                    bytes_sent,
                    duration
                ) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)",
                self.idx.to_string().replace("-", "_")
            );

            let mut stmt = tx.prepare(&insert_sql)?;
            for (
                echo_id,
                correlation_id,
                timestamp,
                destination_path,
                response_code,
                ip,
                payload_size,
                bytes_sent,
                duration,
            ) in tuples
            {
                count += stmt.execute(&[
                    &echo_id,
                    &correlation_id,
                    &timestamp,
                    &destination_path,
                    &response_code,
                    &ip,
                    &payload_size,
                    &bytes_sent,
                    &duration,
                ])?;
            }
        }
        tx.commit()?;
        Ok(count)
    }
}
