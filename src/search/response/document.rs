// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch search document struct.
use serde::Serialize;
use serde_json;
use std::fmt;

/// A document hit
#[derive(Debug, Deserialize, Getters, Serialize, Setters)]
pub struct Document<T> {
    /// The index used.
    #[serde(rename = "_index")]
    index: String,
    /// The type.
    #[serde(rename = "_type")]
    doc_type: String,
    /// The id.
    #[serde(rename = "_id")]
    #[get = "pub"]
    id: String,
    /// The score.
    #[serde(rename = "_score")]
    score: f64,
    /// The source.
    #[serde(rename = "_source")]
    #[get = "pub"]
    source: T,
}

impl<T> fmt::Display for Document<T>
where
    T: Serialize,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match serde_json::to_string(self) {
            Ok(json_str) => write!(f, "{}", json_str),
            Err(e) => write!(f, "{}", e),
        }
    }
}
