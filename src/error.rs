// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `atters` errors
error_chain!{
    foreign_links {
        AddrParse(::std::net::AddrParseError);
        Atter(::atter::Error);
        Io(::std::io::Error);
        Json(::serde_json::Error);
        Libeko(::libeko::Error);
        ParseInt(::std::num::ParseIntError);
        Regex(::regex::Error);
        Rusqlite(::rusqlite::Error);
    }

    errors {
        InvalidEnv {
            description("invalid env argument")
            display("invalid env argument")
        }
        InvalidUsername {
            description("invalid username")
            display("invalid username")
        }
        InvalidPassword {
            description("invalid password")
            display("invalid password")
        }
    }
}
