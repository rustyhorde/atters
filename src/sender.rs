// Copyright (c) 2017 rotor developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch query sender
use atter::{self, Config, Message, RuntimeEnvironment};
use bincode::{serialize, Infinite};
use chrono::{DateTime, Utc};
use error::Result;
use futures::future::result;
use futures::sync::mpsc;
use futures::{Future, Sink};
use libeko::Client;
use log::Logs;
use search::{MslCheckout, ProxyApiCorrId, ProxyApiDest, ProxyBanner, ProxyDC2, ProxyEcsb, Searcher};
use std::path::PathBuf;
use std::thread;
use std::time::Duration;
use uuid::Uuid;

/// Sender type for monitor.
type SenderType = mpsc::UnboundedSender<::std::result::Result<Vec<u8>, ()>>;

/// Repository monitor configuration.
#[derive(Clone, Debug, Getters, Setters)]
pub struct SearchConfig {
    /// The path where the sqlite database lives.
    #[get = "pub"]
    db_path: PathBuf,
    /// The runtime environment.
    #[get = "pub"]
    env: RuntimeEnvironment,
    /// Verbose mode for curl
    #[get = "pub"]
    verbose: bool,
    /// Username for basic auth.
    #[get = "pub"]
    username: String,
    /// Password for basic auth.
    #[get = "pub"]
    password: String,
    /// A date math string.
    #[get = "pub"]
    date_math: Option<String>,
    /// The mpsc sender type.
    #[get]
    tx: SenderType,
    /// The slog logs.
    #[get = "pub"]
    logs: Logs,
    /// The remote handle to the event loop.
    #[get]
    remote_handle: ::tokio_core::reactor::Remote,
}

impl SearchConfig {
    /// Create a new configuration for this monitor.
    pub fn new(
        env: RuntimeEnvironment,
        toml_config: &Config,
        username: &str,
        password: &str,
        date_math: Option<String>,
        tx: SenderType,
        logs: Logs,
        remote_handle: ::tokio_core::reactor::Remote,
    ) -> Self {
        let db_path = atter::db_path(toml_config, &env);
        let verbose = match env {
            RuntimeEnvironment::Prod => toml_config.prod().verbose(),
            RuntimeEnvironment::Stage => toml_config.stage().verbose(),
            RuntimeEnvironment::Test => toml_config.test().verbose(),
            RuntimeEnvironment::Dev => toml_config.dev().verbose(),
        };
        Self {
            db_path,
            env,
            verbose: *verbose,
            username: username.to_string(),
            password: password.to_string(),
            date_math,
            tx,
            logs,
            remote_handle,
        }
    }
}

/// Search thread
pub fn run(config: &Config, search_config: &SearchConfig) -> Result<()> {
    try_trace!(search_config.logs().stdout(), "Starting search thread");

    // Initialize the sqlite db.
    let mut conn = atter::initialize(config, search_config.db_path())?;

    // Initialize the curl client.
    let mut curl_client = Client::new()?;
    curl_client.set_verbose(*search_config.verbose())?;
    curl_client.set_username(search_config.username())?;
    curl_client.set_password(search_config.password())?;

    // First loop flag
    let mut first = true;

    loop {
        let f = result::<(), ()>(Ok(()));
        let tx = search_config.tx().clone();

        // Find the maximum recorded timestamp to generate the initial date range
        // for the Elasticsearch queries. In scope block to drop immutable borrow.
        let date_math = {
            if first {
                if let Some(date_math) = search_config.date_math() {
                    date_math.clone()
                } else {
                    "now-15m/m".to_string()
                }
            } else {
                let before = atter::find_most_recent(&conn)?;
                try_trace!(
                    search_config.logs().stdout(),
                    "Current Maximum Timestamp: {}",
                    before.to_rfc3339()
                );
                let now: DateTime<Utc> = Utc::now();
                let dur = now.signed_duration_since(before);
                let minutes_since = dur.num_minutes();
                try_info!(
                    search_config.logs().stdout(),
                    "Minutes since last recorded entry: {}",
                    minutes_since
                );

                let date_math_str = if minutes_since > 15 && minutes_since < 60 {
                    format!("now-{}m/m", minutes_since)
                } else if minutes_since >= 60 && minutes_since < 1440 {
                    let hours_since = minutes_since / 60;
                    format!("now-{}h/h", hours_since)
                } else if minutes_since >= 1440 {
                    let days_since = minutes_since / 1440;
                    format!("now-{}d/d", days_since)
                } else {
                    "now-15m/m".to_string()
                };
                try_info!(
                    search_config.logs().stdout(),
                    "Elasticsearch date math string: {}",
                    date_math_str
                );
                date_math_str
            }
        };

        // Generate the `mobile-service-layer-checkout` search (this will generate correlation ids)
        let msl_checkout_searcher: MslCheckout = Default::default();
        let msl_request = msl_checkout_searcher.build_request(date_math.clone(), None);
        let (msl_count, correlations_ids, _master_order_ids, sub_order_ids) = msl_checkout_searcher
            .search(
                &mut curl_client,
                &mut conn,
                config,
                search_config,
                &msl_request,
            )?;
        try_info!(
            search_config.logs().stdout(),
            "Recorded 'mobile-service-layer-checkout'"
        );

        // Generate the `proxy-api` by `correlation_id` search.
        let proxy_api_corr_id_searcher: ProxyApiCorrId = Default::default();
        let proxy_api_request = proxy_api_corr_id_searcher
            .build_request(date_math.clone(), Some(correlations_ids.clone()));
        let (api_corr_id_count, _) = proxy_api_corr_id_searcher.search(
            &mut curl_client,
            &mut conn,
            config,
            search_config,
            &proxy_api_request,
        )?;
        try_info!(
            search_config.logs().stdout(),
            "Recorded 'proxy-api' by 'correlation_id'"
        );

        // Generate the `proxy-api` by path search.
        let proxy_api_path_searcher: ProxyApiDest = Default::default();
        let proxy_api_request = proxy_api_path_searcher.build_request(date_math.clone(), None);
        let (api_path_count, _) = proxy_api_path_searcher.search(
            &mut curl_client,
            &mut conn,
            config,
            search_config,
            &proxy_api_request,
        )?;
        try_info!(
            search_config.logs().stdout(),
            "Recorded 'proxy-api' by 'path'"
        );

        // Generate the `proxy-ecsb` search.
        let proxy_ecsb_searcher: ProxyEcsb = Default::default();
        let proxy_ecsb_request =
            proxy_ecsb_searcher.build_request(date_math.clone(), Some(correlations_ids.clone()));
        let (ecsb_count, _) = proxy_ecsb_searcher.search(
            &mut curl_client,
            &mut conn,
            config,
            search_config,
            &proxy_ecsb_request,
        )?;
        try_info!(search_config.logs().stdout(), "Recorded 'proxy-ecsb'");

        // Generate the `proxy-banner` search
        let proxy_banner_searcher: ProxyBanner = Default::default();
        let proxy_banner_request =
            proxy_banner_searcher.build_request(date_math.clone(), Some(sub_order_ids.clone()));
        let (banner_count, _) = proxy_banner_searcher.search(
            &mut curl_client,
            &mut conn,
            config,
            search_config,
            &proxy_banner_request,
        )?;
        try_info!(search_config.logs().stdout(), "Recorded 'proxy-banner'");

        // Generate the `proxy-dc2` search
        let proxy_dc2_searcher: ProxyDC2 = Default::default();
        let proxy_dc2_request =
            proxy_dc2_searcher.build_request(date_math.clone(), Some(correlations_ids.clone()));
        let (dc2_count, _) = proxy_dc2_searcher.search(
            &mut curl_client,
            &mut conn,
            config,
            search_config,
            &proxy_dc2_request,
        )?;
        try_info!(search_config.logs().stdout(), "Recorded 'proxy-dc2'");

        let mut message_str = String::from("\n");
        message_str.push_str(&format!(
            "Added {} 'mobile-service-layer-checkout' rows",
            msl_count
        ));
        message_str.push('\n');
        message_str.push_str(&format!(
            "Added {} 'proxy-api' by correlation id rows",
            api_corr_id_count
        ));
        message_str.push('\n');
        message_str.push_str(&format!(
            "Added {} 'proxy-api' by path rows",
            api_path_count
        ));
        message_str.push('\n');
        message_str.push_str(&format!("Added {} 'proxy-ecsb' rows", ecsb_count));
        message_str.push('\n');
        message_str.push_str(&format!("Added {} 'proxy-banner' rows", banner_count));
        message_str.push('\n');
        message_str.push_str(&format!("Added {} 'proxy-dc2' rows", dc2_count));

        let mut message: Message = Default::default();
        message.set_uuid(Uuid::new_v4());
        message.set_message(message_str);

        try_info!(search_config.logs().stdout(), "{}", message);
        let msg_clone = message.clone();

        search_config.remote_handle().spawn(|_| {
            f.then(move |_res| {
                let encoded = serialize(&msg_clone, Infinite).expect("");
                tx.send(Ok(encoded)).then(|tx| match tx {
                    Ok(_tx) => Ok(()),
                    Err(_e) => Err(()),
                })
            })
        });

        // Sleep until the interval has passed.
        try_trace!(search_config.logs().stdout(), "Sleeping"; "interval" => 150_000);
        thread::sleep(Duration::from_millis(150_000));

        if first {
            first = false;
        }
    }
}
