// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch Query Structs
mod mslcheckout;
mod proxyapi;
mod proxybanner;
mod proxydc2;
mod proxyecsb;
mod request;
mod response;

pub use self::mslcheckout::MslCheckout;
pub use self::proxyapi::{ProxyApiCorrId, ProxyApiDest};
pub use self::proxybanner::ProxyBanner;
pub use self::proxydc2::ProxyDC2;
pub use self::proxyecsb::ProxyEcsb;
pub use self::request::{Bool, ConstantScore, Filter, Param, Query, Range, RequestBody, Timestamp};
pub use self::response::Matches;

use atter::Config;
use error::Result;
use libeko::Client;
use rusqlite::Connection;
use sender::SearchConfig;

pub trait Searcher {
    type ResultTuple;
    type InsertTuple;

    fn build_request(&self, date_math: String, correlation_ids: Option<Vec<String>>)
        -> RequestBody;

    fn search(
        &self,
        client: &mut Client,
        conn: &mut Connection,
        config: &Config,
        search_config: &SearchConfig,
        request: &RequestBody,
    ) -> Result<Self::ResultTuple>;

    fn insert(&self, conn: &mut Connection, tuples: Vec<Self::InsertTuple>) -> Result<i32>;
}

/// Scroll Request
#[derive(Debug, Serialize, Setters)]
pub struct Scroll {
    /// Scroll Timeout
    // #[set = "pub"]
    scroll: String,
    /// Scroll ID
    // #[set = "pub"]
    scroll_id: String,
}
