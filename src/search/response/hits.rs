// Copyright (c) 2018 atters developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Elasticsearch search hits structs.
use search::response::Document;
use serde::Serialize;
use serde_json;
use std::fmt;

/// Document hits in search results
#[derive(Debug, Deserialize, Getters, Serialize)]
pub struct Hits<T> {
    /// Total documents found.
    #[get = "pub"]
    total: usize,
    /// Max score across all documents.
    #[serde(skip_serializing_if = "Option::is_none")]
    max_score: Option<f64>,
    /// Documents
    #[get = "pub"]
    hits: Vec<Document<T>>,
}

impl<T> fmt::Display for Hits<T>
where
    T: Serialize,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match serde_json::to_string(self) {
            Ok(json_str) => write!(f, "{}", json_str),
            Err(e) => write!(f, "{}", e),
        }
    }
}
