select
    datetime(pb.timestamp / 1000, 'unixepoch', 'localtime'),
    mslc2.correlation_id,
    mslc2.profile_id,
    pb.order_id
from proxy_banner pb
inner join
    (
        select mslc.sub_order_id
        from mobile_service_layer_checkout mslc
        where
            endpoint = "CREATE"
            and mslc.response_code = 200
        except
        select mslc1.sub_order_id
        from mobile_service_layer_checkout mslc1
        where
            endpoint = "RELEASE"
            and mslc1.response_code = 200
    ) as oid
        on pb.order_id = oid.sub_order_id
        and pb.response_code = 200
inner join mobile_service_layer_checkout mslc2
    on mslc2.sub_order_id = oid.sub_order_id
order by pb.timestamp;

select datetime(pb.timestamp / 1000, 'unixepoch', 'localtime'), mslc2.correlation_id, mslc2.profile_id, pb.order_id from proxy_banner pb inner join (select mslc.sub_order_id from mobile_service_layer_checkout mslc where endpoint = "CREATE" and mslc.response_code = 200 except select mslc1.sub_order_id from mobile_service_layer_checkout as mslc1 where endpoint = "RELEASE" and mslc1.response_code = 200) as oid on pb.order_id = oid.sub_order_id and pb.response_code = 200 inner join mobile_service_layer_checkout mslc2 on mslc2.sub_order_id = oid.sub_order_id order by pb.timestamp;
select count(*) from proxy_banner pb inner join (select mslc.sub_order_id from mobile_service_layer_checkout mslc where endpoint = "CREATE" and mslc.response_code = 200 except select mslc1.sub_order_id from mobile_service_layer_checkout as mslc1 where endpoint = "RELEASE" and mslc1.response_code = 200) as oid on pb.order_id = oid.sub_order_id and pb.response_code = 200 inner join mobile_service_layer_checkout mslc2 on mslc2.sub_order_id = oid.sub_order_id order by pb.timestamp;